from flask.cli import FlaskGroup

from project import app, db, NotesData
from flask import request
import click

cli = FlaskGroup(app)

@cli.command("create_db")
def create_db():
    db.create_all()
    db.session.commit()

@cli.command("drop_db")
def create_db():
    db.drop_all()
    db.session.commit()


if __name__ == "__main__":
    cli()
