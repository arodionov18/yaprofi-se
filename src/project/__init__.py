import os

from werkzeug.utils import secure_filename
import enum
import requests
from flask_apispec import FlaskApiSpec, doc

from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for,
    abort
)
from flask_sqlalchemy import SQLAlchemy
import json

app = Flask(__name__)
app.config.from_object("project.config.Config")
db = SQLAlchemy(app)

docs = FlaskApiSpec(app)

@app.errorhandler(404)
def resource_not_found(e):
    return jsonify(error=str(e)), 404

@app.errorhandler(500)
def internal_error(e):
    return jsonify(error=str(e)), 500

class NotesData(db.Model):
    __tablename__ = "notes"

    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String, nullable=True)
    content = db.Column(db.String)

    def __init__(self, title: str, content: str):
        self.title = title
        self.content = content

@doc(description="Create a note")
@app.route("/notes", methods=["POST"])
def add_note():
    body = request.json
    title = body.get('title', "")
    content = body.get('content')
    if not content:
        abort(404, description="No content field in request")
    print("ALL_OK", flush=True)
    note = NotesData(title, content)
    try:
        db.session.add(note)
        print("is_ok?", flush=True)
        db.session.flush()
        print("still_OK", flush=True)
        db.session.commit()
    except Exception as e:
        abort(500, description=f"Internal server error, {str(e)}")
    return jsonify({"id": note.id, "title": note.title, "content": note.content})

def get_title(title: str, content: str):
    if title == "":
        N = int(os.getenv("TITLE_CONTENT_LENGTH", 0))
        return content[:N]
    return title

@app.route("/notes", methods=["GET"])
def get_nodes():
    param = request.args.get("query")
    try:
        query = []
        if param:
            query = db.session.query(NotesData).filter((param in NotesData.title) | (param in NotesData.content)).all()
        else:
            query = db.session.query(NotesData).all()
    except:
        abort(500, description="Internal server error")
    return jsonify(list(map(lambda x: {"id": x.id, "title": get_title(x.title, x.content), "content": x.content}, query)))

@app.route("/notes/<id>", methods=["GET"])
def get_node(id: int):
    try:
        query = db.session.query(NotesData).get(id)
    except:
        abort(500, description="Internal server error")
    if not query:
        abort(404, description="Note with given id is not found")
    return jsonify({"id": query.id, "title": get_title(query.title, query.content), "content": query.content})

@app.route("/notes/<id>", methods=["PUT"])
def put_node(id: int):
    body = request.json
    title = body.get('title', "")
    content = body.get('content')
    try:
        query = db.session.query(NotesData).get(id)
    except:
        abort(500, description="Internal server error")
    if not query:
        abort(404, description="Note with given id is not found")
    try:
        query.title = title
        query.content = content
        db.session.commit()
    except:
        abort(500, description="Internal server error")
    return ""

@app.route("/notes/<id>", methods=["DELETE"])
def delete_node(id: int):
    try:
        query = db.session.query(NotesData).filter_by(id=id).delete()
        db.session.commit()
    except:
        abort(500, description="Internal server error")
    return ""

docs.register(add_note)
docs.register(get_nodes)
docs.register(get_node)
docs.register(put_node)
docs.register(delete_node)